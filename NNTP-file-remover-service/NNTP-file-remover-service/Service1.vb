Imports System.ServiceProcess
Imports System
Imports System.io

Public Class Service1
    Inherits System.ServiceProcess.ServiceBase

#Region " Component Designer generated code "

    Public Sub New()
        MyBase.New()

        ' This call is required by the Component Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

    'UserService overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    ' The main entry point for the process
    <MTAThread()> _
    Shared Sub Main()
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase

        ' More than one NT Service may run within the same process. To add
        ' another service to this process, change the following line to
        ' create a second service object. For example,
        '
        '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
        '
        ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1}

        System.ServiceProcess.ServiceBase.Run(ServicesToRun)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    ' NOTE: The following procedure is required by the Component Designer
    ' It can be modified using the Component Designer.  
    ' Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        '
        'Service1
        '
        Me.ServiceName = "Service1"

    End Sub

#End Region

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Dim objTmr As New System.Timers.Timer
        objTmr.Interval = 1000 ' one second
        objTmr.Enabled = True ' setter timer til sant
        objTmr.AutoReset = True

        Console.WriteLine("Starting delete nntp-messages")
        EventLog1.WriteEntry("In OnStart")

        AddHandler objTmr.Elapsed, AddressOf objtmr_elapsed

    End Sub

    Private Sub objTmr_elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs)
        Dim objtime As New TimeSpan(12, 0, 0)
        If TimeSpan.Compare(DateTime.Now.TimeOfDay, objtime) >= 0 Then
            Call doDelete()
        End If
    End Sub
    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        Me.Timer1.Enabled = False
        EventLog1.WriteEntry("In OnStop.")

    End Sub

    Protected Overrides Sub OnContinue()
        EventLog1.WriteEntry("In OnContinue.")
    End Sub

    Public Sub New()
        MyBase.New()
        InitializeComponent()

        ' Sjekker om vi har logfil eller noe slikt. dersom den ikke eksisterer s� legger vi den til
        If Not System.Diagnostics.EventLog.SourceExists("MySource") Then
            System.Diagnostics.EventLog.CreateEventSource("MySource", "MyNewLog")

        End If

        eventlog1.source = "MySource"
        eventlog1.log = "MyNewLog"
    End Sub

    Sub doDelete()
        ' Denne koden skal loope gjennom c:\temp\ mappen og sletter filer som er eldre enn fire timer f.eks.
        Dim folder As String
        Dim time As String
        Dim numFiles As Integer



        Dim fc As Integer
        folder = "c:\temp\"

        Dim di As New DirectoryInfo(folder)

        Dim fi As FileInfo() = di.GetFiles("*.xml") ' vi skal kun slette xml-filer. 

        Console.WriteLine("The following files exist and in the current directory:")

        Dim fiTemp As FileInfo

        Dim txtLog As String
        Dim hour, day, filetime, fileday, DaysSinceCreation, HoursSinceCreation As Integer

        hour = Now.ToString("hh")
        day = Now.ToString("dd")


        For Each fiTemp In fi
            filetime = fiTemp.CreationTime.ToString("hh")
            fileday = fiTemp.CreationTime.ToString("dd")
            filetime = Val(filetime)
            fileday = Val(fileday)
            ' MsgBox("Timer siden filen ble laget: " & filetime - hour)
            ' MsgBox("Dager siden filen ble laget: " & day - fileday)
            DaysSinceCreation = day - fileday
            HoursSinceCreation = filetime - hour
            If HoursSinceCreation >= 1 Then

                txtLog += fiTemp.Name & " (" & fiTemp.CreationTime & ")" & vbCrLf
                fiTemp.Delete()
            End If


        Next
        IO.File.AppendText("c:\temp\deletelog.log").Write(txtLog)
    End Sub

    Private Sub Timer1_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs)
        Call doDelete()
    End Sub
End Class
